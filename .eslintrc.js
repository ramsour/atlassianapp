module.exports = {
    "extends": "google",
	"parserOptions": {
        "ecmaVersion": 6
    },
	"rules": {
		"linebreak-style": "off",
		"max-len":  ["error", 120],
		"eol-last": "off"
	}
};		 
