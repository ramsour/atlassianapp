

/**
 * config data for controlling application's variables
 *
 * @constructor
 */
const config = {
	PORT: 8081,
	MAX_EMOTE_SIZE: 15,
};

module.exports = config;
