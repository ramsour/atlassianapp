
const MessageHandler = require('./message-handler.js');


/**
 * ChatParser class.  Overseer of message parsing.  Creates a unique
 *   parser each time we get a message, keeping concurrent messages
 *   within their own scope.
 *
 * @constructor
 */
let ChatParser = function() {
	let handlers = [];

	/**
	 * Public method for kicking off a string parse.  Saves the created created
	 *   MessageHandler in an array then cleans it up later.  The code functions
	 *   without that layer of control, but as Node is relatively new to me, I
	 *   opted to force the var to go out of scope exactly when I wanted it so.
	 *
	 * @param {string} inputString String to be parsed.
	 * @param {string} resource The obejct holding a .end() method to call with result.
	 */
	this.handleNewGetRequest = function( inputString, resource) {
		let hndlr = new MessageHandler();
		hndlr.processMessage(inputString, resource, requestDone.bind(this, hndlr));
		handlers.push(hndlr);
	};

	/**
	 * Callback used by this.handleNewGetRequest to cleanup saved MessageHandler once
	 *   complete.
	 *
	 * @param {object} hndlr MessageHandler to be forced out of scope.
	 */
	function requestDone(hndlr) {
		let index = handlers.indexOf(hndlr);
		if(index !== -1) {
			handlers.splice(index, 1);
		}
	};
};

module.exports = ChatParser;
