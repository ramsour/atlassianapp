
const request = require('request');
const config = require('./config.js');

/**
 * MessageHandler class.  Processes a single incoming chat message
 *  and produces the string version of the object represented.
 *
 * @constructor
 */
let MessageHandler = function() {
	let result = {};
	let resource = {};
	let requestCount = 0;
	let onComplete = function() {};

	/**
	 * Public interface to parse the imput string, feed the
	 *    results into the res.end method, and then call the
	 *    callback.  Checking validity of URLs is asynchronous.
	 *
	 * @param {string} inputString The string to parse.
	 * @param {object} res The obejct holding a .end() method to call with result.
	 * @param {function} callback The callback to signal completion to owner class.
	 */
	this.processMessage = function(inputString, res, callback) {
		resource = res;
		onComplete = callback;
		parseString(inputString);
		sendResult();
	};

	/**
	 * Adds a mention to results object.
	 *
	 * @param {string} mention The name mentioned.
	 */
	function addMention(mention) {
		if(typeof(result.mentions) === 'undefined') {
			result.mentions = [];
		}
		result.mentions.push(mention);
	};

	/**
	 * Adds an emoticon to results object.
	 *
	 * @param {string} emoticon The name of the emoticon.
	 */
	function addEmoticon(emoticon) {
		if(emoticon.length <= config.MAX_EMOTE_SIZE) {
			if(typeof(result.emoticons) === 'undefined') {
				result.emoticons = [];
			}
			result.emoticons.push(emoticon);
		}
	};

	/**
	 * Adds a URL to results object.
	 *
	 * @param {string} urlIn The URL from the internets.
	 * @param {string} titleIn The title of said URL.
	 */
	function addURL(urlIn, titleIn)	{
		if(typeof(result.links) === 'undefined') {
			result.links = [];
		}
		let linkData = {};
		linkData.url = urlIn.toString();
		linkData.title = titleIn.toString();
		result.links.push(linkData);
	};

	/**
	 * Checks if character at given index in string is alphanumeric.
	 *
	 * @param {string} str The string with character to check.
	 * @param {number} index The index of character to check.
	 * @return {bool} true if character is number or letter.
	 */
	function isAlphanumeric(str, index) {
		code = str.charCodeAt(index);
		return ((code > 47 && code < 58) || // numeric (0-9)
			(code > 64 && code < 91) ||		// upper alpha (A-Z)
			(code > 96 && code < 123)); 	// lower alpha (a-z)
	};

	/**
	 * Parses a word to check for a special character. Used to check for at-symbol or
	 *   open-parenthesis as start of mention or emoticon.  Implementation does not
	 *   require that the tokens be wrapped in spaces.  Ex: "@bob@fred(smile)!" will
	 *   produce two mentions and one emoticon.
	 *
	 * @param {string} inputWord The word to check
	 * @param {string} startChar Special character to look for
	 * @param {function} addFunc Function to call if a valid entry is found
	 * @param {bool} validateEndParen Bool indicating if we need a trailing ) to have a valid entry
	 */
	function parseWord(inputWord, startChar, addFunc, validateEndParen) {
		let loc = inputWord.indexOf(startChar);
		while(loc > -1) {
			let token = '';
			let index = loc+1;
			for( ; index < inputWord.length; index++) {
				if( !isAlphanumeric(inputWord, index) ) {
					break;
				}
				token += inputWord[index];
			}
			if(!validateEndParen || inputWord[index] === ')') {
				addFunc(token);
			}
			loc = inputWord.indexOf(startChar, loc+1);
		}
	};

	/**
	 * Entry point for string parsing, checking for special characters and URLS.
	 *
	 * @param {string} inputString String to be parsed.
	 */
	function parseString(inputString) {
		if(inputString.endsWith('\"') && inputString.indexOf('\"') == 0 ) {
			inputString = inputString.slice(1, -1);
		}

		let words = inputString.split(' ');
		requestCount = words.length;

		words.forEach( function(element) {
			parseWord(element, '@', addMention, false);
			parseWord(element, '(', addEmoticon, true);

			// For now dictating that a URL must be surrounded by spaces, so something
			//  like "check out www.atlassian.com, it's cool" won't work (due to the ',')
			request(element, function(error, response, body) {
				requestCount--;
				if (!error && response.statusCode == 200) {
					let title = body.split('title>');
					addURL(element, title[1].substring(0, title[1].length-2));

					sendResult();
				}
			});
		});
	};

	/**
	 * Callback used to trigger the completion once all URL requests have returned.
	 *
	 */
	function sendResult() {
		if(requestCount === 0) {
			resource.end(JSON.stringify(result));
			onComplete();
		}
	};
};

module.exports = MessageHandler;
