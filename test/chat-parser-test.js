
var chai = require('chai');
var expect = chai.expect; // we are using the "expect" style of Chai

var cp = require('../chat-parser.js');
var chatParser = new cp();


describe('ParseTesting', function() {
	it('should return {} with meaningless input ["nothing"]', function(done) {
		var resource = {
			end : function(text){
				expect(text).to.equal("{}");
				done();
			}
		};
		chatParser.handleNewGetRequest("nothing",resource);
		//chatParser.testNewGetRequest("test",resource, validate, done);
	});
	it('should find mentions ["@chris you around?"]', function(done) {
		var resource = {
			end : function(text){
				expect(text).to.equal('{"mentions":["chris"]}');
				done();
			}
		};
		chatParser.handleNewGetRequest("@chris you around?",resource);
	});
	it('should find emoticons ["Good morning! (megusta) (coffee)"]', function(done) {
		var resource = {
			end : function(text){
				expect(text).to.equal('{"emoticons":["megusta","coffee"]}');
				done();
			}
		};
		chatParser.handleNewGetRequest("Good morning! (megusta) (coffee)",resource);
	});
	it('should find URLs ["http://www.nbcolympics.com"]', function(done) {
		var resource = {
			end : function(text){
				expect(text).to.equal('{"links":[{"url":"http://www.nbcolympics.com","title":"2016 Rio Olympic Games | NBC Olympics"}]}');
				done();
			}
		};
		chatParser.handleNewGetRequest("Olympics are starting soon; http://www.nbcolympics.com",resource);
	});
	it('should find all three with input complex input', function(done) {
		var resource = {
			end : function(text){
				expect(text).to.equal('{"mentions":["bob","john"],"emoticons":["success"],"links":[{"url":"https://twitter.com/jdorfman/status/430511497475670016","title":"Justin Dorfman on Twitter: &quot;nice @littlebigdetail from @HipChat (shows hex colors when pasted in chat). http://t.co/7cI6Gjy5pq&quot;"}]}'); 
				done();
			}
		};
		chatParser.handleNewGetRequest("@bob @john (success) such a cool feature; https://twitter.com/jdorfman/status/430511497475670016",resource);
	});
	it('should exclude emoticons over 15 characters ["bad: (bad4567890123456), good: (good56789012345)"]', function(done) {
		var resource = {
			end : function(text){
				expect(text).to.equal('{"emoticons":["good56789012345"]}');
				done();
			}
		};
		chatParser.handleNewGetRequest("bad: (bad4567890123456), good: (good56789012345)",resource);
	});
	it('should exclude parenthetical notes ["hello (means hi)"]', function(done) {
		var resource = {
			end : function(text){
				expect(text).to.equal('{}');
				done();
			}
		};
		chatParser.handleNewGetRequest("hello (means hi)",resource);
	});
	it('should return alphanumeric portions of tokens ["@bob???!?!? *@fred*, !(hi)?"]', function(done) {
		var resource = {
			end : function(text){
				expect(text).to.equal('{"mentions":["bob","fred"],"emoticons":["hi"]}');
				done();
			}
		};
		chatParser.handleNewGetRequest("@bob???!?!? *@fred*, !(hi)?",resource);
	});
	it('should handle chained items ["(smile)@fred@bob(end)"]', function(done) {
		var resource = {
			end : function(text){
				expect(text).to.equal('{"mentions":["fred","bob"],"emoticons":["smile","end"]}');
				done();
			}
		};
		chatParser.handleNewGetRequest("(smile)@fred@bob(end)",resource);
	});
	it('should handle extra parentheses ["(hi(smile)) (ugh(why weird)"]', function(done) {
	
		var resource = {
			end : function(text){
				expect(text).to.equal('{"emoticons":["smile"]}');
				done();
			}
		};
		chatParser.handleNewGetRequest("(hi(smile)) (ugh(why weird)",resource);
	});
	it('should handle roughly concurrent calls', function(done) {
	
		var dummyResource1 = {end : function(text) {}};
		var dummyResource2 = {end : function(text) {}};
		var resource = {
			end : function(text) {
				expect(text).to.equal('{"links":[{"url":"http://www.nbcolympics.com","title":"2016 Rio Olympic Games | NBC Olympics"}]}');
				done();
			}
		};
		chatParser.handleNewGetRequest("http://www.atlassian.com",dummyResource1);
		chatParser.handleNewGetRequest("http://www.nbcolympics.com",resource);
		chatParser.handleNewGetRequest("https://www.hipchat.com/",dummyResource2);
	});
});
