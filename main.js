
/**
 * See Readme.md for general information.
 */


const express = require('express');
let app = express();

const ChatParser = require('./chat-parser.js');
let chatParser = new ChatParser();

const config = require('./config.js');

// setup method to be called when we get a get request.
app.get('/', function(req, res) {
	chatParser.handleNewGetRequest(req.query.input, res);
});

// start the express server.
app.listen(config.PORT, function() {
	// nothing going on here.  just waiting for a request.
	console.log('Listener ready at localhost:' + config.PORT + '/?input="<chat message>"');
});
