# README #

### What is this repository for? ###

* Quick summary - code test for Atlassian.  


### How do I get set up? ###

*Note, this has only been tested in Windows*

1. Ensure you have node.js and npm installed (recent node should come with npm)
1. Pull down everything in repo
1. Open a command window to the root directory of this project
1. run "npm install" to install dependencies. If your environment is setup for production only (unlikely) you may need to copy the "devDependencies" into "dependencies" in the package.json to get test packages installed correctly.  Try this if the test step fails. 
1. run "npm test" to ensure test cases pass.  
1. run "npm start" to start program
	1. Program will create RESTful API looking input on port 8081 of localhost.  
	1. Syntax is: localhost:8081/?input="<chat message>".  

	
### General Notes ###

* As a  warning, I will mention that I've never written a REST API before and never written anything for Node.js.  While I have done a fair amount of JavaScript programming in my time, I'm far more experienced in C++.  So I expect there to be a few spots in this program that don't follow Node/REST best practices, or are written in more of a C++ style than JavaScript.  
* At my current job we use perforce for our version control.  I believe I've figured out git, but it's new to me, so please excuse any errors in getting you this content.
* If this were a real-life long-term project, I'd probably take the hit of learning GO so as to be in a more rigid language. If that were not an option, my second choice would be Node.js under a wrapper of TypeScript.  I didn't do TS here because it added a layer of complexity to a project I feel I only get one chance at.  


### Design Overview ###

* The instructions are unclear on the exact signature for the API, so I went with expecting **"?input="** as the lead in before the message.  The alternative (localhost:8081/"my message") was sub-optimal as it broke up the message if there was a ? in the middle of it.  Using a POST request rather than a GET is probably even better, but I went with GET as it was more straightforward to develop and test.
* **Files**
	* config.js *data* - Configuration data for application.  Not much of value here in this example, but I like the practice of data driving what I can from outside the code.
	* main.js *logic* - Simply sets up the server and the GET listener.  Calls into ChatParser when it recieves a GET request
	* chat-parser.js *logic* - Overseer class.  All this does is create a new MessageHandler each time a new request comes in. This layer is used to deal with concurrent requests as well as ensuring the MessageHandler stays in scope exactly as long as it should
	* message-handler.js *logic* - This is the brains of the operation.  It takes the input string, splits it into words, then parses each word.  More info on that in Parsing bullet.
	* chat-parser-test.js *test* - Full suite of unit tests for ChatParser.
		* First, why only test this file?  This class only has one public method, which calls the only public method inside message-handler.  So to focus on testing the public interfaces, testing at this level covered the level below.  The level above, main.js, only sets up a server and awaits input.  There are node packages out there that can test these, but that seemed like a lot of bloat for very little value.  If this were production, I would not want a package that fakes a server to test things out.  I would want an actual test server, that my code gets loaded to, and actually auto-tested.  
		* Second, what is tested?  Please look over the test cases as they reflect my understanding of the requirements. I included your example cases, as well as what edge cases I could think of.  
* **Parsing**
	* Each word is parsed independently.  Technically marching the entire string character by character would be *slightly* more efficient than first breaking it into words, but both options are O(n) time.  The single character marching option led to code that was much harder to read and maintain.  I tend to prefer readability and maintainability over slight bumps in performance, but obviously there are times when you have to take the hit to make it faster.
	* URLs are assumed to be the entire word, so if the URL is followed by some form of punctuation, it will not resolve.  If I were writing this as a real application, I would probably try out each word as a URL both as-is, and without any trailing potential-punctuation.  That seemed like significant overkill for this project. 
	* Mentions and Emoticons are not assumed to be the entire word. Here I support either token including punctuation as well as being concatenated  with any other string.
* **Third party packages.**  These will automatically be pulled down when you run "npm install".  My approach was to focus on writing my own business logic, while relying on third parties to handle server/web communication and testing/validation.
	* express - This package sets up the actual REST server.  It creates the server, and provides a simple mechanism for me to declare a GET callback.
	* request - This package handles getting the data from a remote URL.  It can return the body as a string, which I can pull the <title></title> block out of.
	* eslint & eslint-config-google - Static code analysis tool.  Very helpful in a language like JavaScript where features like no-type-safety can get you in trouble.
	* mocha & chai - Test and assertion framework.  These provide the infrastructure for all the unit tests created. 

